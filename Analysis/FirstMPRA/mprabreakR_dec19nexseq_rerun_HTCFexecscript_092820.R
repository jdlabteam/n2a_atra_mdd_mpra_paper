library(data.table)
setDTthreads(24)
source("code/mpraBreakR_R3.6plus_mbrkr2.x-tweaked_hg38dbsnp151_092720_forhtcf24core.R")

mprabreakr.res <- mprabreakR(mpra4mpra.res.obj = "mbq7530_altrefconsistent_from0926rds_formotifbreakrrerun092820.txt",n.null.runs = 10000,rsid_col = "rsid_only",FDR.col = "FDR",logfc.col = "log2FC",hit.FDR = 0.05,,element_col = "rsid_allele",motifbreakr.pthresh = 1E-4,motifbreakr.method = "default",concord.carryover.col.names=c("rsid_only","rsid_allele","log2FC","FDR","tag_snp",report.TF.level=TRUE,report.motif.level=FALSE))

# Save output files for analysis (separate .rmd)


outfilenames <- c("MDD2-N2A-m4m1.3-MBQ7530_hg38dbsnp151-motifbreakR_allSNPresults_092830.txt","MDD2-N2A-m4m1.3-MBQ7530_hg38dbsnp151-motifbreakR_MPRAsignifSNPresults_092830.txt","MDD2-N2A-m4m1.3-MBQ7530_hg38dbsnp151-motifbreakR_TFchangeConcordance_anyeffectstr_092830.txt","MDD2-N2A-m4m1.3-MBQ7530_hg38dbsnp151-motifbreakR_TFchangeConcordance_strongFxonly_092830.txt","MDD2-N2A-m4m1.3-MBQ7530_hg38dbsnp151-motifbreakR_TF-SNP-freqs_anyeffectstr_092830.txt","MDD2-N2A-m4m1.3-MBQ7530_hg38dbsnp151-motifbreakR_TF-SNP-freqs_strongFxonly_092830.txt")

i<-1
for (i in c(1:6)){
  write.table(x=mprabreakr.res[[i]],file=paste0("output/",outfilenames[i]),sep='\t',row.names=FALSE,col.names=TRUE,quote=FALSE)
}