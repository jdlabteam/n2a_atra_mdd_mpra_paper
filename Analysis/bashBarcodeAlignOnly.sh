#!/usr/bin/env bash

### place unpacked fastq files into a single directory, with their filenames each on a separate line in
### a file called "newsamplenames.txt", stored in the same directory. 

## cd your_fastq_directory_here/

upstream=GTCGAC
downstream=GCGATCGC

for i in $(cat newsamplenames.txt)
do
	cat $i | sed -n '/^[ATCGN]\{150\}/p' > "$i"_clean.txt
	grep -v "GGGGGGGGGGGGGG" "$i"_clean.txt > "$i"_final.txt
	sed -n 's/^.*'"$upstream"'\([[:upper:]]\{10\}\)'"$downstream"'.*$/\1/p' "$i"_final.txt > "$i"_foundBCs.txt
done

## place all _foundBCs.txt files in a directory titled foundBCs, with the DNA reads
## named such that they are alphabetically first and contain "DNA" in the filename.
## then proceed into the R single-condition analysis scripts to use the mpra4MPRA
## function v1.3.1 to get counts of each barcode and analyze allelic differences
## within condition. 