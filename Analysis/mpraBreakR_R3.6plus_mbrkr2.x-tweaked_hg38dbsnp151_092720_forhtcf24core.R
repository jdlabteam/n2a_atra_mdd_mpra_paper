## START: mprabreakR function (motifbreakR with negative SNP permutations to control for motif hit/motif disruption frequency in functional vs. nonfunctional snps)

# this is a heavy duty script, especially the snps.from.rsids() portion, for some reason. the motifbreakr runs can be parallelized but take 2 min per core on my computer. this will need to be dispatched on the HTCF to get a reasonable number of null draws to get empirical p-values for each TF's occurence in the + set.

## parameters:
# mpra4mpra.res.obj: main output table from the mpra4mpra function

# n.null.runs: number of times to choose n= n significantly differential SNPs at random and run those through motifbreakr, tabulating TF occurences to assess empirical enrichment in positive SNPs vs nulls

#rsid_col: column name in the MPRA result object for the column in which ONLY the SNP's rsid is located (these are pulled out based on FDR to extract genomic data for to use in motifbreakr.)

#FDR.col: column name for the FDR (or other corrected p-value) in the MPRA result object.

#hit.FDR: threshold for defining functional SNPs to test, based on FDR.col, to pass to motifbreakr

#concord.carryover.col.names: a vector with the mpra4mpra result object's column names for the snp identifier column, snp_allele identifier column, log2FC measure column, and FDR column. (these are the four columns needed to subset and shuffle snps/alleles and assign them random, significant log2FC).

### METHOD NOTE: CONCORDANCE ###
# achieved by grabbing n=(n significant snps) at random, assign a randomly selected allele of each of those SNPs one of the significant snp alleles' logFC value, and assign the -logFC to the other allele of the random SNP.
# then, test those selected SNPs for motif changes
# assess how frequently each motif changes AND the rate at which the expression increasing allele is the motif-improving allele. repeat several iterations of this to determine a null concordance rate per TF to compare with the concordance rates of motifs affected by the actual functional SNPs.

## outputs (motif, TF, or both levels as specified in function call):
# [[1]] : motifbreakr results for significantly differential SNPs merged back to the MPRA results.
# [[2]] : 
#   # Concordance rates at the level of single MOTIF matches. Number of SNPs (one or both alleles) matching a particular motif (e.g., a single motif defined for a TF by one of several groups--thus a SNP may match, e.g. 3 different motifs for RARA, each of which is listed separately).
#   # Hits.nmotif.concordant.fx, corresponds to SNPs with concordant motif change effects and expression change effects.
#   # Additional columns represent the ratio of these two values when the significant logFC values are assigned to random SNPs (significant OR nonsignificant SNPs).
# [[3]]: Concordance rates on a per TF basis.
#   # hits.n.snp : the number of SNPs matched to a TF by any number of its alleles and/or any number of motifs (e.g., if a SNP has one allele that matches 10 motifs for 1 TF, that's only counted as 1 TF•SNP pair) and
#   # hits.n.snp.conc : the number of SNPs with ≥1 allele matching 1+ motifs for a TF and matched on allelic MPRA and motif effect directions
#   # additional columns represent the ratio of these two values when the significant logFC values are assigned to random SNPs (significant OR nonsignif snps).
# [[4]]: Number of SNPs matching a motif, regardless of concordance between MPRA and motif match direction of effect. Additional columns are the same value, determined for randomly selected, equally-sized sets of nonsignificant (i.e., putatively nonregulatory) SNPs.
# [[5]]: Number of SNPs paired to a given TF for any number of its motifs, any number of SNP alleles matching at a nonzero level (same thing as [[3]] above; again, just a measure of how many unique SNPs were assigned the TF in any way shape or form), and values computed likewise for equally-sized, randomly-selected groups of nonsignificant (i.e. putatively nonregulatory) SNPs.

### Why collect these two metrics (allele-motif matches in [[2]] and [[4]] vs. SNP-TF matches in [[3]] and [[5]])?
# Motifs come from different datasets and sources -- so cases where a sequence matches multiple versions of a motif may be more likely to represent biological signal (i.e. a component common to motifs derived from distinct datasets)
# In the concordance scenario, different motifs may be more predictive of direction of function (e.g., for bifunctional TFs, a particular motif may be more predictive of a site where the TF will act as an enhancer rather than a repressor, or vice versa.) 
# The focus of this project, though, is to consider RECURRENTLY AFFECTED TFS, so it's also of interest to know how frequently functional vs. random snps get assigned to a TF (to see whether that TF is enriched among functional variants in the disease loci).

library(BiocParallel)

mprabreakR <- function(
  mpra4mpra.res.obj="omni.mbq7530.1.3.txt",
  n.null.runs=2,
  rsid_col="rsid_only",
  FDR.col="FDR",
  logfc.col="log2FC",
  hit.FDR=0.001,
  report.motif.level=FALSE,
  report.TF.level=TRUE,
  element_col="rsid_allele",
  concord.carryover.col.names=c("rsid_only","rsid_allele","log2FC","FDR","tag_snp"),
  motifbreakr.pthresh=1E-4,
  motifbreakr.method="default"){
  
  require(GenomicRanges)
  require(BSgenome)
  require(motifbreakRBJM528)
  require(MotifDb)
  require(SNPlocs.Hsapiens.dbSNP151.GRCh38)
  require(BSgenome.Hsapiens.UCSC.hg38)
  require(BiocParallel)
  require(data.table)
  require(matrixStats)
  require(BiocParallel)
  
  ### load result table
  mpra4mpra.res.obj <- fread(mpra4mpra.res.obj)
  
  ### make an allele-pair-to-SNP lookup table for subsetting motif data
  rsid_allele_refcols <- c(rsid_col,element_col)
  mpra_rsid_alleles_a <- unique(mpra4mpra.res.obj[,..rsid_allele_refcols])
  mpra_rsid_alleles_b <- unique(mpra4mpra.res.obj[,..rsid_allele_refcols])
  mpra_rsid_alleles_a[,mpra.allele.1:=gsub(get(element_col),pattern="^.*=(.*)$",replacement="\\1")]
  mpra_rsid_alleles_b[,mpra.allele.2:=gsub(get(element_col),pattern="^.*=(.*)$",replacement="\\1")]
  mpra_rsid_alleles_a[,c(element_col):=NULL]
  mpra_rsid_alleles_b[,c(element_col):=NULL]
  
  mpra_rsid_alleles <- merge(mpra_rsid_alleles_a,mpra_rsid_alleles_b,by=rsid_col)
  rm(mpra_rsid_alleles_a,mpra_rsid_alleles_b)
  
  #remove controls and single allele copies of rows (no need to subset further to rs.*)
  mpra_rsid_alleles <- mpra_rsid_alleles[mpra.allele.1!=mpra.allele.2]
  mpra_rsid_alleles_ref <- as.data.frame(matrix(ncol=3),nrow=0)
  rsidsref <- unique(mpra_rsid_alleles[,get(rsid_col)])
  i<-1
  for (i in c(1:length(rsidsref))){
    mpra_rsid_alleles_ref[i,] <- mpra_rsid_alleles[get(rsid_col)==rsidsref[i]][1,]
  }
  mpra_rsid_alleles_ref <- as.data.table(mpra_rsid_alleles_ref)
  setnames(mpra_rsid_alleles_ref,old=c("V1","V2","V3"),new=names(mpra_rsid_alleles))
  
  rm(mpra_rsid_alleles,i,rsid_allele_refcols,rsidsref)
  
  ### subset data and get to work
  
  mpra.hit.snps <- unique(mpra4mpra.res.obj[get(FDR.col)<hit.FDR,get(rsid_col)])
  mpra.null.snps <- unique(mpra4mpra.res.obj[get(FDR.col)>hit.FDR,get(rsid_col)])
  mpra.all.snps <- unique(unique(mpra4mpra.res.obj[!is.na(get(FDR.col)),get(rsid_col)]))
  
  break.all <- snps.from.rsid(rsid=mpra.all.snps,dbSNP=SNPlocs.Hsapiens.dbSNP151.GRCh38,search.genome = BSgenome.Hsapiens.UCSC.hg38)
  
  # unload dbsnp since its taking up space
  detach("package:SNPlocs.Hsapiens.dbSNP151.GRCh38",unload=TRUE)
  gc()
  #subset to SNPs in dbsnp 151
  ## subset query SNP lists to those actually in the dbSNP-derived set
  mpra.hit.snps <- mpra.hit.snps[which(mpra.hit.snps%in%break.all$SNP_id)]
  mpra.null.snps <- mpra.null.snps[which(mpra.null.snps%in%break.all$SNP_id)]
  mpra.all.snps <- mpra.all.snps[which(mpra.all.snps%in%break.all$SNP_id)]
  # run once for ALL hits. (subset results table for MPRA SNP hits --> out; then cycle through subsetting the result table)
  actual.nsnp.in <- length(mpra.hit.snps)
  all.broken <- motifbreakR(snpList = break.all,pwmList= motifbreakR_motif,threshold =motifbreakr.pthresh,filterp = TRUE,method = motifbreakr.method,BPPARAM = MulticoreParam(workers=24))
  # unload BSgenome, motifbreakR, motifDb to conserve memory.
  detach("package:BSgenome.Hsapiens.UCSC.hg38",unload=TRUE)
  detach("package:motifbreakRBJM528",unload=TRUE)
  detach("package:MotifDb",unload=TRUE)
  gc()
  # convert results to data table
  all.breaks.dt <- as.data.table(all.broken)
  all.breaks.dt[,c(rsid_col):=gsub(names(all.broken),pattern="^(.*):.*$",replacement="\\1")]
  
  
  # subset to allele pairs that were represented in MPRA; merge MPRA results
  all.breaks.dt <- merge(all.breaks.dt,mpra_rsid_alleles_ref,by=rsid_col)
  all.breaks.dt <- all.breaks.dt[((mpra.allele.1==REF|mpra.allele.1==ALT)&(mpra.allele.2==REF|mpra.allele.2==ALT))]
  all.breaks.dt[,mpra.allele.1:=NULL]
  all.breaks.dt[,mpra.allele.2:=NULL]
  
  # fix NA values in the geneSymbol column by replacing with the corresponding motif name given by the source (e.g. HOMER) database
  all.breaks.dt[is.na(geneSymbol),geneSymbol:=providerName]
  
  # set up scoring for concordance
  all.breaks.dt[,ref.gain.loss:=scoreRef-scoreAlt]
  all.breaks.dt[,alt.gain.loss:=scoreAlt-scoreRef]
  # join critical bits of mpra result table
  all.breaks.dt.w.mpra <- merge(unique(mpra4mpra.res.obj[,..concord.carryover.col.names]),all.breaks.dt,by=rsid_col,allow.cartesian=TRUE)
  # for concordance purposes, get the MPRA snp allele out of the element ID 
  all.breaks.dt.w.mpra[,mpra.allele:=gsub(x=get(element_col),pattern="^.*=(.*)$",replacement="\\1")]
  
  #clean up
  rm(break.all)
  
  # annotate concordance in real data
  ## if the alt allele results in weakened binding and lowered expression, then log2FC and alt.gain.loss will both be - ; likewise both positive for gain
  all.breaks.dt.w.mpra[mpra.allele==ALT,concordant:=ifelse(alt.gain.loss*get(logfc.col)>0,yes=TRUE,no=FALSE)]
  ### same as above for ref alleles
  all.breaks.dt.w.mpra[mpra.allele==REF,concordant:=ifelse(ref.gain.loss*get(logfc.col)>0,yes=TRUE,no=FALSE)]
  ## NOT POSSIBLE IN MOTIFBREAKR 2 (no alleleRef / alleleAlt fields): remove alleles where its base is never found at the given position in the motif
  # hit.breaks.dt.w.mpra <- hit.breaks.dt.w.mpra[!((mpra.allele==REF & alleleRef==0)|(mpra.allele==ALT & alleleAlt==0))]
  
  # set aside all SNP and MPRA hit SNP result table copies for output
  all.breaks.dt.mpra.out <- copy(all.breaks.dt.w.mpra)
  hit.breaks.dt.mpra.out <- copy(all.breaks.dt.w.mpra[get(rsid_col)%in%mpra.hit.snps])
  
  ### make match per snp and match per motif tables to expand with shuffled data, starting from the hit data, for both 'weak' and 'strong' effect SNPs in the motifbreakR output field
  if (report.motif.level){
    # strong hits only, motifwise
    motifmatchcols <- c(rsid_col,"providerId","dataSource","effect")
    # subset down to one row per SNP-motif instance, so that tallies will be number of SNPs in the table matching a motif
    hits.motif.tmp <- unique(hit.breaks.dt.mpra.out[,..motifmatchcols])
    # extract tally of strong hits and all hits for each providerId (motif) 
    hits.motif.strong.tmp <- unique(as.data.table(table(hits.motif.tmp[effect=="strong",providerId,by=get(rsid_col)]))[N!=0])
    hits.motif.strong.tmp <- hits.motif.strong.tmp[,.N,by=providerId]
    hits.motif.tmp <- unique(as.data.table(table(hits.motif.tmp[,providerId,by=get(rsid_col)]))[N!=0])
    hits.motif.tmp <- hits.motif.tmp[,.N,by=providerId]
    setnames(hits.motif.strong.tmp,c(1,2),c("motifID","strongfx.nmotif.matches.inHitSNPs"))
    setnames(hits.motif.tmp,c(1,2),c("motifID","anyfx.nmotif.matches.inHitSNPs"))
  }
  
  if (report.TF.level){
    # per TF•SNP, 'effect' weak and strong
    tfmatchcols <- c(rsid_col,"geneSymbol","effect")
    hits.tf.tmp <- unique(hit.breaks.dt.mpra.out[,..tfmatchcols])
    hits.tf.strong.tmp <- unique(as.data.table(table(hits.tf.tmp[effect=="strong",geneSymbol,by=get(rsid_col)]))[N!=0])
    hits.tf.strong.tmp <- hits.tf.strong.tmp[,.N,by=geneSymbol]
    hits.tf.tmp <- unique(as.data.table(table(hits.tf.tmp[,geneSymbol,by=get(rsid_col)]))[N!=0])
    hits.tf.tmp <- hits.tf.tmp[,.N,by=geneSymbol]
    setnames(hits.tf.strong.tmp,c(1,2),c("TF","hits.nsnp.strongfx.TFmatches"))
    setnames(hits.tf.tmp,c(1,2),c("TF","hits.nsnp.anyfx.TFmatches"))
  }
  
  # concordance testing
  if(report.motif.level){
    ### per motif concordance tables (join to motif tables from above), both all and strong effect 
    motifconccols <- c(rsid_col,"providerId","dataSource","effect","concordant")
    hits.conc.tmp <- unique(hit.breaks.dt.mpra.out[,..motifconccols])
    hits.conc.strong.tmp <- unique(as.data.table(table(hits.conc.tmp[effect=="strong"&concordant==TRUE,providerId,by=get(rsid_col)]))[N!=0])
    hits.conc.strong.tmp <- hits.conc.strong.tmp[,.N,by=providerId]
    hits.conc.tmp <- unique(as.data.table(table(hits.conc.tmp[concordant==TRUE,providerId,by=get(rsid_col)]))[N!=0])
    hits.conc.tmp <- hits.conc.tmp[,.N,by=providerId]
    setnames(hits.conc.strong.tmp,c(1,2),c("motifID","strongfx.concordantSNPs.nmotif.inHitSNPs"))
    setnames(hits.conc.tmp,c(1,2),c("motifID","anyfx.concordantSNPs.nmotif.inHitSNPs"))
    # make conc tables for aggeregating sim results
    conc.any <- merge(hits.motif.tmp,hits.conc.tmp,by="motifID",all.x=TRUE)
    conc.strong <- merge(hits.motif.strong.tmp,hits.conc.strong.tmp,by="motifID",all.x=TRUE)
    rm(hits.conc.tmp,hits.conc.strong.tmp)
  }
  if (report.TF.level){
    # as above, for TFs
    tfconccols <- c(rsid_col,"geneSymbol","effect","concordant")
    tfs.conc.tmp <- unique(hit.breaks.dt.mpra.out[,..tfconccols])
    # make comprehensive SNP x TF tables (one for strong concs, one for all concs) counting how many times that pair occurs. we don't care about the number, we just want to know whether a TF was assigned to a SNP AT LEAST once. this generates one row per TF-SNP; we then tabulate the number of rows listed for a TF to get the number of snps that matched that TF
    tfs.conc.strong.tmp <- unique(as.data.table(table(tfs.conc.tmp[effect=="strong"&concordant==TRUE,geneSymbol,by=get(rsid_col)]))[N!=0])
    tfs.conc.strong.tmp <- tfs.conc.strong.tmp[,.N,by=geneSymbol]
    tfs.conc.tmp <- unique(as.data.table(table(tfs.conc.tmp[concordant==TRUE,geneSymbol,by=get(rsid_col)]))[N!=0])
    tfs.conc.tmp <- tfs.conc.tmp[,.N,by=geneSymbol]
    setnames(tfs.conc.tmp,c(1,2),c("TF","anyfx.concordant.TFsnpMatches.inHitSNPs"))
    setnames(tfs.conc.strong.tmp,c(1,2),c("TF","anyfx.concordant.TFsnpMatches.inHitSNPs"))
    conc.tf.any <- merge(hits.tf.tmp,tfs.conc.tmp,all.x=TRUE,by="TF")
    conc.tf.strong <- merge(hits.tf.strong.tmp,tfs.conc.strong.tmp,all.x=TRUE,by="TF")
  }
  # add code to merge in the fed in result table and examine concordance by shuffling expression values and comparing to actual data, analogous to the portion below
  signifFCs <- unique(abs(mpra4mpra.res.obj[get(rsid_col)%in%mpra.hit.snps,get(logfc.col)]))
  elems.tmp <- unique(mpra4mpra.res.obj[!is.na(get(logfc.col)),.(get(rsid_col),get(element_col))])
  setnames(elems.tmp,c(1,2),c(rsid_col,element_col))
  elems <- elems.tmp[get(rsid_col)%in%mpra.all.snps]
  
  #cleanup
  rm(elems.tmp)
  gc()
  
  ## iterative shuffling, concordance calling
  i<-1
  for (i in c(1:n.null.runs)){
    print(paste0("Beginning concordance run ",i))
    randos.tmp <- sample(elems[,get(element_col)],size=length(signifFCs),replace = FALSE)
    randos.init <- as.data.table(cbind(randos.tmp,signifFCs))
    setnames(randos.init,c(1,2),c(element_col,logfc.col))
    randos.init[,c(rsid_col):=gsub(x=get(element_col),pattern="^.*_(rs.*)=.*$",replacement="\\1")]
    #cleanup
    rm(randos.tmp)
    gc()
    # ugh, have to coerce back to numeric..
    randos.init[,c(logfc.col):=as.numeric(get(logfc.col))]
    
    tmp <- elems[get(rsid_col)%in%randos.init[,get(rsid_col)]&!(get(element_col)%in%randos.init[,get(element_col)])]
    tmpmergecols <- c(rsid_col,logfc.col)
    tmp <- merge(randos.init[,..tmpmergecols],tmp,by=rsid_col)
    #set logfc to opposite value for other allele of the snp pair.
    tmp[,c(logfc.col):=-get(logfc.col)]
    randos <- rbind(randos.init,tmp)
    
    rm(tmp,tmpmergecols,randos.init)
    randos[,mpra.allele:=gsub(x=get(element_col),pattern="^.*=(.*)$",replacement="\\1")]
    
    rando_rsids <- unique(randos[,get(rsid_col)])
    # fetch the results WITHOUT the joined mpra data (since we want to join shuffled data for concordance calling here)
    randos.broken.dt <- all.breaks.dt[get(rsid_col) %in% rando_rsids]
    randos.broken.strong.dt <- randos.broken.dt[effect=="strong"]
    
    randos.broken.dt[,ref.gain.loss:=scoreRef-scoreAlt]
    randos.broken.dt[,alt.gain.loss:=scoreAlt-scoreRef]
    randos.broken.strong.dt[,ref.gain.loss:=scoreRef-scoreAlt]
    randos.broken.strong.dt[,alt.gain.loss:=scoreAlt-scoreRef]
    # merge back the shuffled data
    randos.broken.mpra <- merge(randos.broken.dt,randos,by=rsid_col,all.x=TRUE,allow.cartesian=TRUE)
    randos.broken.strong.mpra <- merge(randos.broken.strong.dt,randos,by=rsid_col,all.x=TRUE,allow.cartesian=TRUE)
    
    #cleanup
    rm(randos.broken.dt,randos.broken.strong.dt)
    ## call concordance as above
    randos.broken.mpra[mpra.allele==ALT,concordant:=ifelse(alt.gain.loss*get(logfc.col)>0,yes=TRUE,no=FALSE)]
    randos.broken.mpra[mpra.allele==REF,concordant:=ifelse(ref.gain.loss*get(logfc.col)>0,yes=TRUE,no=FALSE)]
    randos.broken.strong.mpra[mpra.allele==ALT,concordant:=ifelse(alt.gain.loss*get(logfc.col)>0,yes=TRUE,no=FALSE)]
    randos.broken.strong.mpra[mpra.allele==REF,concordant:=ifelse(ref.gain.loss*get(logfc.col)>0,yes=TRUE,no=FALSE)]
    # create number of concordant matches table for the nulls and join to hit results
    if (report.motif.level){
      # motif matches 
      randos.motif.tmp <- unique(randos.broken.mpra[,..motifmatchcols])
      randos.motif.strong.tmp <- unique(as.data.table(table(randos.motif.tmp[effect=="strong",providerId,by=get(rsid_col)]))[N!=0])
      randos.motif.strong.tmp <- randos.motif.strong.tmp[,.N,by=providerId]
      randos.motif.tmp <- unique(as.data.table(table(randos.motif.tmp[,providerId,by=get(rsid_col)]))[N!=0])
      randos.motif.tmp <- randos.motif.tmp[,.N,by=providerId]
      setnames(randos.motif.strong.tmp,old=c(1,2),new=c("motifID","n.shufs.motifmatch"))
      setnames(randos.motif.tmp,c(1,2),c("motifID","n.shufs.motifmatch"))
      # motif concordance
      randos.motif.conc.tmp <- unique(randos.broken.mpra[,..motifconccols])
      randos.motif.conc.strong.tmp <- unique(as.data.table(table(randos.motif.conc.tmp[effect=="strong"&concordant==TRUE,providerId,by=get(rsid_col)]))[N!=0])
      randos.motif.conc.strong.tmp <- randos.motif.conc.strong.tmp[,.N,by=providerId]
      randos.motif.conc.tmp <- unique(as.data.table(table(randos.motif.conc.tmp[concordant==TRUE,providerId,by=get(rsid_col)]))[N!=0])
      randos.motif.conc.tmp <- randos.motif.conc.tmp[,.N,by=providerId]
      setnames(randos.motif.conc.tmp,c(1,2),c("motifID","n.shufs.conc"))
      setnames(randos.motif.conc.strong.tmp,old=c(1,2),new=c("motifID","n.shufs.conc"))
      # merge to calculate conc/all ratio
      shuf.motif.conc.tmp <- merge(randos.motif.tmp,randos.motif.conc.tmp,by="motifID",all.x=TRUE)
      shuf.motif.conc.strong.tmp <- merge(randos.motif.strong.tmp,randos.motif.conc.strong.tmp,by="motifID",all.x=TRUE)
      rm(randos.motif.strong.tmp,randos.motif.conc.strong.tmp,randos.motif.tmp,randos.motif.conc.tmp)
      # calculate ratio
      shuf.motif.conc.tmp[,newcol:=n.shufs.conc/n.shufs.motifmatch]
      shuf.motif.conc.strong.tmp[,newcol:=n.shufs.conc/n.shufs.motifmatch]
      shuf.motif.conc.tmp[,n.shufs.motifmatch:=NULL]
      shuf.motif.conc.tmp[,n.shufs.conc:=NULL]
      shuf.motif.conc.strong.tmp[,n.shufs.motifmatch:=NULL]
      shuf.motif.conc.strong.tmp[,n.shufs.conc:=NULL]
      # set name for the ratio to be merged into the corresponding motif result table
      setnames(shuf.motif.conc.tmp,old="newcol",new=paste0("shuf.motif.anyfx.concrate_",i))
      setnames(shuf.motif.conc.strong.tmp,old="newcol",new=paste0("shuf.motif.strongfx.concrate_",i))
      conc.any <- merge(conc.any,shuf.motif.conc.tmp,by="motifID",all.x=TRUE)
      conc.strong <- merge(conc.strong,shuf.motif.conc.strong.tmp,by="motifID",all.x=TRUE)
      #cleanup
      rm(shuf.motif.conc.tmp,shuf.motif.conc.strong.tmp)
      gc()
    }
    
    if (report.TF.level){
      # same for TF level 
      randos.TF.tmp <- unique(randos.broken.mpra[,..tfmatchcols])
      randos.TF.strong.tmp <- unique(as.data.table(table(randos.TF.tmp[effect=="strong",geneSymbol,by=get(rsid_col)]))[N!=0])
      randos.TF.strong.tmp <- randos.TF.strong.tmp[,.N,by=geneSymbol]
      randos.TF.tmp <- unique(as.data.table(table(randos.TF.tmp[,geneSymbol,by=get(rsid_col)]))[N!=0])
      randos.TF.tmp <- randos.TF.tmp[,.N,by=geneSymbol]
      setnames(randos.TF.strong.tmp,old=c(1,2),new=c("TF","n.shufs.TFmatch"))
      setnames(randos.TF.tmp,c(1,2),c("TF","n.shufs.TFmatch"))
      # TF concordance
      randos.TF.conc.tmp <- unique(randos.broken.mpra[,..tfconccols])
      randos.TF.conc.strong.tmp <- unique(as.data.table(table(randos.TF.conc.tmp[effect=="strong"&concordant==TRUE,geneSymbol,by=get(rsid_col)]))[N!=0])
      randos.TF.conc.strong.tmp <- randos.TF.conc.strong.tmp[,.N,by=geneSymbol]
      randos.TF.conc.tmp <- unique(as.data.table(table(randos.TF.conc.tmp[concordant==TRUE,geneSymbol,by=get(rsid_col)]))[N!=0])
      randos.TF.conc.tmp <- randos.TF.conc.tmp[,.N,by=geneSymbol]
      setnames(randos.TF.conc.tmp,c(1,2),c("TF","n.shufs.conc"))
      setnames(randos.TF.conc.strong.tmp,old=c(1,2),new=c("TF","n.shufs.conc"))
      # merge to calculate conc/all ratio
      shuf.TF.conc.tmp <- merge(randos.TF.tmp,randos.TF.conc.tmp,by="TF",all.x=TRUE)
      shuf.TF.conc.strong.tmp <- merge(randos.TF.strong.tmp,randos.TF.conc.strong.tmp,by="TF",all.x=TRUE)
      rm(randos.TF.strong.tmp,randos.TF.conc.strong.tmp,randos.TF.tmp,randos.TF.conc.tmp)
      # calculate ratio
      shuf.TF.conc.tmp[,newcol:=n.shufs.conc/n.shufs.TFmatch]
      shuf.TF.conc.strong.tmp[,newcol:=n.shufs.conc/n.shufs.TFmatch]
      shuf.TF.conc.tmp[,n.shufs.TFmatch:=NULL]
      shuf.TF.conc.tmp[,n.shufs.conc:=NULL]
      shuf.TF.conc.strong.tmp[,n.shufs.TFmatch:=NULL]
      shuf.TF.conc.strong.tmp[,n.shufs.conc:=NULL]
      # set name for the ratio to be merged into the corresponding TF result table
      setnames(shuf.TF.conc.tmp,old="newcol",new=paste0("shuf.TF.anyfx.concrate_",i))
      setnames(shuf.TF.conc.strong.tmp,old="newcol",new=paste0("shuf.TF.strongfx.concrate_",i))
      
      conc.tf.any <- merge(conc.tf.any,shuf.TF.conc.tmp,by="TF",all.x=TRUE)
      conc.tf.strong <- merge(conc.tf.strong,shuf.TF.conc.strong.tmp,by="TF",all.x=TRUE)
      #cleanup
      rm(shuf.TF.conc.strong.tmp,shuf.TF.conc.tmp,null.conc,randos.broken.mpra)
      gc()
    }
    
    
  }
  
  
  ######## PART 2 #########
  #########################
  
  # get null frequencies of strong and strong+weak effect matches from across equal sized null snp sets
  # first, initialize mpra hit result tallies to add nulls to
  
  if (report.motif.level){
    matchfreqs <- hits.motif.tmp
    strong.matchfreqs <- hits.motif.strong.tmp
    setnames(matchfreqs,c(1,2),c("motifID","anyfx.nHitsnp.motifmatches"))
    setnames(strong.matchfreqs,c(1,2),c("motifID","strongfx.nHitsnp.motifmatches"))
    rm(hits.motif.tmp,hits.motif.strong.tmp)
  }
  
  if (report.TF.level){
    tffreqs <- hits.tf.tmp
    strong.tffreqs <- hits.tf.strong.tmp
    setnames(tffreqs,c(1,2),c("TF","anyfx.nHitsnp.TFmatches"))
    setnames(tffreqs,c(1,2),c("TF","strongfx.nHitsnp.TFmatches"))
    rm(hits.tf.tmp,hits.tf.strong.tmp)
  }
  
  
  
  i <- 1
  
  for (i in 1:n.null.runs){
    print(paste0("Beginning frequencies run ",i))
    null.sample.snps <- sample(mpra.null.snps,size=actual.nsnp.in,replace = FALSE)
    null.sample.broken <- all.breaks.dt[get(rsid_col)%in%null.sample.snps]
    rm(null.sample.snps)
    if (report.motif.level){
      
      ## create number of matches table for the nulls at all/strong only levels for motifs and tfs, and join to hit results
      # motif level
      null.motifs.tmp <- unique(null.sample.broken[,..motifmatchcols])
      null.motifs.strong.tmp <- unique(as.data.table(table(null.motifs.tmp[effect=="strong",providerId,by=get(rsid_col)]))[N!=0])
      null.motifs.strong.tmp <- null.motifs.strong.tmp[,.N,by=providerId]
      null.motifs.tmp <- as.data.table(table(null.motifs.tmp[,providerId]))
      setnames(null.motifs.tmp,c(1,2),c("motifID",paste0("anyfx.nullSNP.motifmatches_",i)))
      setnames(null.motifs.strong.tmp,c(1,2),c("motifID",paste0("strongfx.nullSNP.motifmatches_",i)))
      matchfreqs <- merge(matchfreqs,null.motifs.tmp,by="motifID",all.x=TRUE)
      strong.matchfreqs <- merge(strong.matchfreqs,null.motifs.strong.tmp,by="motifID",all.x=TRUE)
      rm(null.motifs.tmp,null.motifs.strong.tmp)
      gc()
    }
    
    if (report.TF.level){
      null.tfs.tmp <- unique(null.sample.broken[,..tfmatchcols])
      null.tfs.strong.tmp <- unique(as.data.table(table(null.tfs.tmp[effect=="strong",geneSymbol,by=get(rsid_col)]))[N!=0])
      null.tfs.strong.tmp <- null.tfs.strong.tmp[,.N,by=geneSymbol]
      null.tfs.tmp <- unique(as.data.table(table(null.tfs.tmp[,geneSymbol,by=get(rsid_col)]))[N!=0])
      null.tfs.tmp <- null.tfs.tmp[,.N,by=geneSymbol]
      setnames(null.tfs.tmp,c(1,2),c("TF",paste0("anyfx.nullSNP.TFmatches_",i)))
      setnames(null.tfs.strong.tmp,c(1,2),c("TF",paste0("strongfx.nullSNP.TFmatches_",i)))
      tffreqs <- merge(tffreqs,null.tfs.tmp,by="TF",all.x=TRUE)
      strong.tffreqs <- merge(strong.tffreqs,null.tfs.strong.tmp,by="TF",all.x=TRUE)
      rm(null.tfs.tmp,null.tfs.strong.tmp,null.sample.broken)
      gc()
    }
    # cleanup
  }
  ## add motifs' corresponding gene symbols to the motif output tables
  if (report.motif.level){
    conc.any <- merge.data.table(conc.any,unique(all.breaks.dt.mpra.out[,.(providerId,geneSymbol)]),by.x="motifID",by.y="providerId")
    conc.strong <- merge.data.table(conc.strong,unique(all.breaks.dt.mpra.out[,.(providerId,geneSymbol)]),by.x="motifID",by.y="providerId")
    matchfreqs <- merge.data.table(matchfreqs,unique(all.breaks.dt.mpra.out[,.(providerId,geneSymbol)]),by.x="motifID",by.y="providerId")
    strong.matchfreqs <- merge.data.table(strong.matchfreqs,unique(all.breaks.dt.mpra.out[,.(providerId,geneSymbol)]),by.x="motifID",by.y="providerId")
  }
  
  if (report.TF.level==TRUE & report.motif.level==FALSE){
    returnlist <- list(all.breaks.dt.mpra.out,hit.breaks.dt.mpra.out,conc.tf.any,conc.tf.strong,tffreqs,strong.tffreqs)
    names(returnlist) <- c("AllmpraSNP.Results","Hit.SNP.Results","ConcordRates.AnyEffect.ByTF","ConcordRates.StrongEffect.ByTF","MatchCounts.AnyEffect.ByTF","MatchCounts.StrongEffect.ByTF")
  }
  else if (report.TF.level==FALSE & report.motif.level==TRUE){
    returnlist <- list(all.breaks.dt.mpra.out,hit.breaks.dt.mpra.out,conc.any,conc.strong,matchfreqs,strong.matchfreqs)
    names(returnlist) <- c("AllmpraSNP.Results","Hit.SNP.Results","ConcordRates.AnyEffect.ByMotif","ConcordRates.StrongEffect.ByMotif","MatchCounts.AnyEffect.ByMotif","MatchCounts.StrongEffect.ByMotif")
  }
  else{
    returnlist <- list(all.breaks.dt.mpra.out,hit.breaks.dt.mpra.out,conc.any,conc.strong,conc.tf.any,conc.tf.strong,matchfreqs,strong.matchfreqs,tffreqs,strong.tffreqs)
    names(returnlist) <- c("AllmpraSNP.Results","Hit.SNP.Results","ConcordRates.AnyEffect.ByMotif","ConcordRates.StrongEffect.ByMotif","ConcordRates.AnyEffect.ByTF","ConcordRates.StrongEffect.ByTF","MatchCounts.AnyEffect.ByMotif","MatchCounts.StrongEffect.ByMotif","MatchCounts.AnyEffect.ByTF","MatchCounts.StrongEffect.ByTF")
  }
  
  return(returnlist)
}
