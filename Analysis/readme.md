## mpra4mpra: Script for processing MPRA barcode counts and assessing significance with custom filters and three options for allele-differential calling.
### Analysis version: 1.3.1; 08/21/20.
#### Input objects:
1. Counts directory (barcode count files only!), OR counts in an .RDS format with a column named identically to the library metadata table--said column is used to merge in the counts with the metadata for processing, and is specified by "meta.BC.col" 
2. Filepath for the DNA barcode counts (ONLY if not be in the counts directory) DONE
3. Metadata sheet (containing at bare minimum the barcode sequences as read from the sequencing data, identifiers by which to group a set of barcodes to an identical regulatory sequence, and identifiers by which to group those barcode sets to a superset (e.g., an rsID, an allele, and a barcode column))
##### Regarding inputs, user must define:
1. Column containing barcode sequences (by name or by column index)
2. Column identifying 'elements' (to group barcodes to an individual sequence or ONE variant thereof)
3. Column identifying 'elem.group' (to group alleles into SNPs, for e.g.)
4. **Assumes DNA counts are coming from a single sample** (i.e. from input plasmid or virus) 

#### Input parameters (analytic):
1. RNA barcode seq count threshold for inclusion
2. DNA barcode seq count threshold for inclusion
3. Minimum number of barcodes above threshold per element for inclusion
4. Use counts or cpm?
5. Normalize expression to that of a 'basal' set of barcodes? (if so, declare basals' identifier used in 'elements')
6. Determine element expression as 
	a) aggregated barcodes sum (log2(sum(element RNA barcodes) / sum (element DNA barcodes)) in each replicate)
	b) aggregated barcodes mean (mean ( log2(element RNA barcode/element DNA barcode) ) ), or
	c) using repeated-measures lmm with barcode-level expression values as input
7. Bootstrapping -- simulate *bootstrap.iter* "allelic" tests between randomly selected sets of *bootstrap.nBCs* subsetted from among a specified single element *bootstrap.elem.id* (e.g. basal promoter set), and generate a vector of test statistics (t-values or F values) for the corresponding analysis method to correct p-values (using the *qvalue package*). This appears to be robust to the number of barcodes used for each simulation when the number of simulated tests is large; however, for accuracy's sake a good value to use would be the median number of barcodes analyzed among actual single-allele elements in the library.

#### Filtering steps, in order:
1. Filter out barcodes < minimum DNA count threshold (*'DNA.thresh'*)
2. Filter out barcode SETS from all replicates if the above step leaves fewer than the specified minimum number of barcodes per element for analysis (*'nBC.thresh'*) 
3. Filter out singular barcodes < minimum RNA count threshold (separately for each replicate) (*'RNA.thresh'*)
4. Filter out outlier barcode expression values (defined by number of standard deviations from the barcode's group (i.e., CRE or allele or etc) mean (*'outlierBC.nSDs'*) from each replicate separately. **Note that mean barcode-group expression values are subsequently removed, as the removal of outliers will change the resultant means when recalculated.**
5. From ALL samples, drop barcodes expressed in fewer than the minimum number of replicates specified for barcode inclusion in analysis (*'nRep.BCthresh'*).
6. From INDIVIDUAL samples, drop barcode sets that are below the minimum number of barcodes required in a given replicate for analysis of that element in that replicate (*'nBC.thresh'*, same parameter as number 2 above).

#### Output parameters:.
1. FDR threshold for elements to be returned. Default: 0.05, using Benjamini-Hochberg correction.
2. Report null elements? (output object of elements for which RNA barcodes are 0 in >1 replicate though DNA counts passed threshold.) Default: TRUE
3. nulls.n : if ^ = TRUE, how many replicates need to have shown the null effect (i.e. RNA/DNA = 0 with DNA > 0) to be reported back? (default=1)
4. Samplewise expression for nulls T/F. (T = return a null elements object with columns of expression for each sample so long as at least *nulls.n* samples showed null effect, plus mean expression of that element across the replicates. If report = TRUE but samplewise.nulls = FALSE, just returns elements with 1+ null and the mean expression). Default: FALSE
5. Stepwise dropout? Reports back number of barcodes, alleles, and allele pairs lost at each filtering step. 