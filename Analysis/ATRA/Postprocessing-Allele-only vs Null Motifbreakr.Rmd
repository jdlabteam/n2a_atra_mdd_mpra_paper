---
title: Motifbreakr Rerun from 092820-TF Enrichment in Allele-Main vs No-Effect SNPs
  from Analysis 4 LMM
author: "Bernie Mulvey"
date: "9/30/2020"
output: html_document
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(fig.height = 10,fig.width = 7,include = FALSE)
knitr::opts_chunk$set(fig.width=7,fig.height=10)

require(ggplot2)
require(data.table)
require(Biostrings)
theme_set(theme_bw()+theme(axis.text.x = element_text(size = 14), axis.title.x = element_text(size = 16), axis.text.y = element_text(size = 14), axis.title.y = element_text(size =16), plot.title = element_text(size = 20,hjust=0.5), strip.text = element_text(size=18), legend.text = element_text(size=10), legend.title = element_text(size=11)))
```

---
title: Motifbreakr Rerun from 092820-TF Enrichment in DrugXAllele SNPs relative to allele-only
  SNPs from Analysis 4 LMM SNPs 
author: "Bernie Mulvey"
date: "8/22/2020"
output: html_document
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(fig.height = 10,fig.width = 7,include = FALSE)
knitr::opts_chunk$set(fig.width=7,fig.height=10)

require(ggplot2)
require(data.table)
require(Biostrings)
theme_set(theme_bw()+theme(axis.text.x = element_text(size = 14), axis.title.x = element_text(size = 16), axis.text.y = element_text(size = 14), axis.title.y = element_text(size =16), plot.title = element_text(size = 20,hjust=0.5), strip.text = element_text(size=18), legend.text = element_text(size=10), legend.title = element_text(size=11)))
```

## used mprabreakr 092820 version,, which skipped the motif-level analyses altogether.

### Post processing, adapted from Dec'19 mprabreakr analysis
### Load in the individual sheets of output (from the HTCF run)
```{r}

avn.allsnp.mbout <- fread("Previously Generated Final Results/motifbreakr followup 2-Allele-only SNPs vs No effect SNPs/allelevsnullMDD2-ATRA-ALLELEvsNULL_hg38dbsnp151-motifbreakR_allSNPresults_092830.txt")
avn.intsnp.mbout <- fread("Previously Generated Final Results/motifbreakr followup 2-Allele-only SNPs vs No effect SNPs/allelevsnullMDD2-ATRA-ALLELEvsNULL_hg38dbsnp151-motifbreakR_MPRAsignifSNPresults_092830.txt")
avn.intsnp.TFfreq.any <- fread("Previously Generated Final Results/motifbreakr followup 2-Allele-only SNPs vs No effect SNPs/allelevsnullMDD2-ATRA-ALLELEvsNULL_hg38dbsnp151-motifbreakR_TF-SNP-freqs_anyeffectstr_092830.txt")
avn.intsnp.TFfreq.strong <- fread("Previously Generated Final Results/motifbreakr followup 2-Allele-only SNPs vs No effect SNPs/allelevsnullMDD2-ATRA-ALLELEvsNULL_hg38dbsnp151-motifbreakR_TF-SNP-freqs_strongFxonly_092830.txt")
avn.intsnp.TFconc.any <- fread("Previously Generated Final Results/motifbreakr followup 2-Allele-only SNPs vs No effect SNPs/allelevsnullMDD2-ATRA-ALLELEvsNULL_hg38dbsnp151-motifbreakR_TFchangeConcordance_anyeffectstr_092830.txt")
avn.intsnp.TFconc.strong <- fread("Previously Generated Final Results/motifbreakr followup 2-Allele-only SNPs vs No effect SNPs/allelevsnullMDD2-ATRA-ALLELEvsNULL_hg38dbsnp151-motifbreakR_TFchangeConcordance_strongFxonly_092830.txt")

avn.mbreak.allres <- list(avn.allsnp.mbout,avn.intsnp.mbout,avn.intsnp.TFconc.any,avn.intsnp.TFconc.strong,avn.intsnp.TFfreq.any,avn.intsnp.TFfreq.strong)
names(avn.mbreak.allres) <- c("Mbreakr_Output_Table_All_SNPs","Mbreakr_Output_Table_Interaction_SNPs","Anystrength_TF_Concordance_InHits","Strong_TF_Concordance_InHits","Anystrength_TF_Freq_InHits","Strong_TF_Freq_Inhits")

# saveRDS(avn.mbreak.allres,"Previously Generated Final Results/motifbreakr followup 2-Allele-only SNPs vs No effect SNPs/AllelevsNull-All 092820 Rerun Outputs.RDS")
rm(list=grep(ls(),pattern="avn.intsnp",value=TRUE),avn.allsnp.mbout)
```


```{r}
# set NAs to 0 in the simulated result tables (list items 3-6)

res.tabs <- list()

i<-3
for (i in c(3:length(avn.mbreak.allres))){
  tmp <- avn.mbreak.allres[[i]]
  setnafill(tmp,typ="const",fill=0,cols=c(2:ncol(tmp)))
  res.tabs[[i-2]] <- tmp
}
rm(tmp,i)
# add short names to list contents to keep track of which result table is which
names(res.tabs)[1:4] <- c("Anystrength_TF_Concordance_InHits","Strong_TF_Concordance_InHits","Anystrength_TF_Freq_InHits","Strong_TF_Freq_Inhits")
```

## Determine where the hit values for each row of each table falls relative to the distribution of the same motif/TF in the nulls or shuffles

```{r}
# TF freq tables are 3 and 4 in the res tables list; all are 10002 columns
i<-3
for (i in c(3,4)){
  curtab <- as.matrix(res.tabs[[i]])
  emp.pval.tmp <- rep(1,times=nrow(curtab)) 
  j<-1
  for (j in c(1:nrow(curtab))){
    row.f <- ecdf(curtab[j,c(3:10002)])
    emp.pval.tmp[j] <- row.f(curtab[j,2])
  }
  curtab <- as.data.table(curtab[,c(1,2)])
  curtab[,emp.pctile:=emp.pval.tmp]
  res.tabs[[i]] <- curtab
}

# all conc tables are results 1:2 @ 10003 col ea
i<-1
for (i in c(1,2)){
  curtab <- as.matrix(res.tabs[[i]])
  emp.pval.tmp <- rep(1,times=nrow(curtab))
  j<-1
  for (j in c(1:nrow(curtab))){
    row.f <- ecdf(curtab[j,c(4:10003)])
    row.concratio <- as.numeric(curtab[j,3])/as.numeric(curtab[j,2])
    emp.pval.tmp[j] <- row.f(row.concratio)
  }
  curtab <- as.data.table(curtab[,c(1:3)])
  curtab[,emp.pctile:=emp.pval.tmp]
  res.tabs[[i]] <- curtab
}
rm(i,j,curtab,row.f,row.concratio,emp.pval.tmp)

# # re add names in case blown away by all this
names(res.tabs)[1:4] <- c("Anystrength_TF_Concordance_InHits","Strong_TF_Concordance_InHits","Anystrength_TF_Freq_InHits","Strong_TF_Freq_Inhits")

# combine all 4 TF level (as opposed to motif level) results
all.tf.res <- as.data.frame(matrix(ncol=5,nrow=0))

names(all.tf.res) <- c("TF","nhits.match","nhits.conc","emp.pctile","analysis.type")

i<-1

for (i in c(1:4)){
  nrow.res.init <- nrow(all.tf.res)
  curtab <- as.data.frame(res.tabs[[i]])
  j<-1
  if (ncol(curtab)==3){
    for(j in c(1:nrow(curtab))){
      all.tf.res[(nrow.res.init+j),1] <- curtab[j,1]
      all.tf.res[(nrow.res.init+j),2] <- curtab[j,2]
      all.tf.res[(nrow.res.init+j),3] <- NA
      all.tf.res[(nrow.res.init+j),4] <- curtab[j,3]
      all.tf.res[(nrow.res.init+j),5] <- names(res.tabs)[i]
    }
  }
  else{
    for (j in c(1:nrow(curtab))){
      all.tf.res[(nrow.res.init+j),1] <- curtab[j,1]
      all.tf.res[(nrow.res.init+j),2] <- curtab[j,2]
      all.tf.res[(nrow.res.init+j),3] <- curtab[j,3]
      all.tf.res[(nrow.res.init+j),4] <- curtab[j,4]
      all.tf.res[(nrow.res.init+j),5] <- names(res.tabs)[i]
    }
    nrow.res.fin <- nrow(all.tf.res)
  }
}

rm(nrow.res.init,nrow.res.fin,i,j,curtab)
```

Convert percentiles to 2-sided p-values
```{r}
all.tf.res <- as.data.table(all.tf.res)
all.tf.res[,pval:=ifelse(emp.pctile>0.5,yes = 2*(1-emp.pctile),no=2*emp.pctile)]
# lowest possible p-value is 1/10000 (number of sims) == 0.0001
all.tf.res[pval==0,pval:=0.0001]
# multiple testing
all.tf.res[,FDR:=p.adjust(pval,method="BH")]
```

### Spot check that these are correct now
```{r}
all.tf.res[TF=="RARG"]
length(unique(avn.mbreak.allres[[2]][geneSymbol=="RARG",rsid_only]))
# 5
length(unique(avn.mbreak.allres[[2]][geneSymbol=="RARG"&concordant==TRUE,rsid_only]))
# 5
length(unique(avn.mbreak.allres[[2]][geneSymbol=="RARG"&effect=="strong",rsid_only]))
# 5
```
## OK great mbreakr can count now!

```{r}
View(all.tf.res[FDR<0.05])
rxrs <- c("RARA","RARB","RARG","RXRA","RXRB","RXRG","RORA","RORB","RORG")

# get fields re: significant retinoid receptors of interest
retinoid.signifs <- all.tf.res[FDR<0.05 & TF %in% rxrs]
# map back to SNPs for hits
intxnsnp.fullres <- avn.mbreak.allres[[2]]

rxrb.snps <- unique(intxnsnp.fullres[geneSymbol=="RXRB",rsid_only])
retinoid.signifs[TF=="RXRB"&analysis.type=="nulls.tf.any",SNPs:=paste0(rxrb.snps,collapse=",")]
rxrb.strongsnps <- unique(intxnsnp.fullres[geneSymbol=="RXRB"&effect=="strong",rsid_only])
retinoid.signifs[TF=="RXRB"&analysis.type=="nulls.tf.strong",SNPs:=paste0(rxrb.strongsnps,collapse=",")]

rara.snps <- unique(intxnsnp.fullres[geneSymbol=="RARA",rsid_only])
retinoid.signifs[TF=="RARA"&analysis.type=="nulls.tf.any",SNPs:=paste0(rara.snps,collapse=",")]

rarb.snps <- unique(intxnsnp.fullres[geneSymbol=="RARB",rsid_only])
retinoid.signifs[TF=="RARB"&analysis.type=="nulls.tf.any",SNPs:=paste0(rarb.snps,collapse=",")]
rarb.strongsnps <- unique(intxnsnp.fullres[geneSymbol=="RARB"&effect=="strong",rsid_only])
retinoid.signifs[TF=="RARB"&analysis.type=="nulls.tf.strong",SNPs:=paste0(rarb.strongsnps,collapse=",")]

rarg.snps <- unique(intxnsnp.fullres[geneSymbol=="RARG",rsid_only])
retinoid.signifs[TF=="RARG"&analysis.type=="nulls.tf.any",SNPs:=paste0(rarg.snps,collapse=",")]
rarg.strongsnps <- unique(intxnsnp.fullres[geneSymbol=="RARG"&effect=="strong",rsid_only])
retinoid.signifs[TF=="RARG"&analysis.type=="nulls.tf.strong",SNPs:=paste0(rarg.strongsnps,collapse=",")]

rxrg.snps <- unique(intxnsnp.fullres[geneSymbol=="RXRG",rsid_only])
retinoid.signifs[TF=="RXRG"&analysis.type=="nulls.tf.any",SNPs:=paste0(rxrg.snps,collapse=",")]
rxrg.strongsnps <- unique(intxnsnp.fullres[geneSymbol=="RXRG"&effect=="strong",rsid_only])
retinoid.signifs[TF=="RXRG"&analysis.type=="nulls.tf.strong",SNPs:=paste0(rxrg.strongsnps,collapse=",")]

rora.strongsnps <- unique(intxnsnp.fullres[geneSymbol=="RORA"&effect=="strong",rsid_only])
retinoid.signifs[TF=="RORA",SNPs:=paste0(rora.strongsnps,collapse = ",")]

rm(list=grep(x=ls(),pattern=".*\\.snps$",value=TRUE))
rm(list=grep(x=ls(),pattern=".*\\.strongsnps$",value=TRUE))
rm(row.concratio,res.tabs)
```

## Save results
```{r}
write.table(all.tf.res,"Previously Generated Final Results/motifbreakr followup 2-Allele-only SNPs vs No effect SNPs/TFwise results for allele-main effect SNPs compared to no-effect SNPs 093020.txt",sep='\t',row.names=FALSE,col.names=TRUE,quote=FALSE)

write.table(retinoid.signifs,"Previously Generated Final Results/motifbreakr followup 2-Allele-only SNPs vs No effect SNPs/Signif Retinoid Receps in 1+ of the 4 TF analyses of allele-main fx SNPs vs no-effect SNPs and their corresponding permutation p,FDR and the contributing SNPs 093020.txt",sep='\t',quote=FALSE,row.names=FALSE,col.names=TRUE)

write.table(all.tf.res[FDR<0.05],"Previously Generated Final Results/motifbreakr followup 2-Allele-only SNPs vs No effect SNPs/All TFs Significantly Enriched for Allele-Perturbed Matches or Allele-Cocnordant MPRA-PWM fx solely among allele-main effect SNPs relative to no-effect SNPs 093020.txt",sep='\t',col.names=TRUE,row.names=FALSE,quote=FALSE)

write.table(all.tf.res[FDR<0.05&nhits.match>=2,TF],"Previously Generated Final Results/motifbreakr followup 2-Allele-only SNPs vs No effect SNPs/TFs with 2+ allele-main effect interaction SNPs and enriched for allele-perturbed matches or concordance relative to no-effect SNPs 093020.txt",sep='/t',row.names=FALSE,col.names=TRUE,quote=FALSE)
```

### One last thing that will be useful is to have a table that maps all of the disrupted TF motifs matched to each SNP (since the results have one row per-SNP TF pair).

```{r}
mbreak.allelevsnull.TFs.bySNP <- as.data.table(unique(avn.mbreak.allres[[2]][,rsid_only]))
setnames(mbreak.allelevsnull.TFs.bySNP,1,"rsid_only")
mbreak.allelevsnull.TFs.bySNP[,allTFs:=""]
mbreak.allelevsnull.TFs.bySNP[,strongTFs:=""]
mbreak.allelevsnull.TFs.bySNP[,allTFs:=sapply(X = mbreak.allelevsnull.TFs.bySNP$rsid_only,FUN=function(x){return(paste0(unique(avn.mbreak.allres[[2]][rsid_only %in% x, geneSymbol]),collapse=","))})]

mbreak.allelevsnull.TFs.bySNP[,strongTFs:=sapply(X = mbreak.allelevsnull.TFs.bySNP$rsid_only,FUN=function(x){return(paste0(unique(avn.mbreak.allres[[2]][effect=="strong"][rsid_only %in% x, geneSymbol]),collapse=","))})]

write.table(mbreak.allelevsnull.TFs.bySNP,"Previously Generated Final Results/motifbreakr followup 2-Allele-only SNPs vs No effect SNPs/Lists of TF Motif Matches for each Allele main-effect SNP, with any or only 'strong' allelic effect on motif match 093020.txt",sep="\t",row.names=FALSE,col.names=TRUE,quote=FALSE)

rm(rxrs,RAR.interxnsnps,retinoid.signifs,mbreak.allelevsnull.TFs.bySNP,all.tf.res,avn.mbreak.allres)
```